package com.para.noika;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		loadPage();
		((Button)findViewById(R.id.buttonRefresh)).setOnClickListener(this);
		((Button)findViewById(R.id.buttonExit)).setOnClickListener(this);
	}
	
	public void loadPage() {
		WebView myBrowser = (WebView)findViewById(R.id.webViewMain);
		WebSettings webSettings = myBrowser.getSettings();
		webSettings.setJavaScriptEnabled(true);
		myBrowser.loadUrl("file:///android_asset/index.html");
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.buttonRefresh)
			loadPage();
		if (v.getId() == R.id.buttonExit)
			finish();
		
	}

}
